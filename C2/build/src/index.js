"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const body_parser_1 = __importDefault(require("body-parser"));
const mockedData = __importStar(require("../resources/MockedListData.json"));
const app = express_1.default();
const port = 8080;
app.use(body_parser_1.default.json());
app.use(body_parser_1.default.urlencoded({ extended: false }));
app.get("/", (request, response) => {
    response.send("Hello!");
});
app.get("/lists", (req, res) => {
    res.send(mockedData.lists);
});
app.post("/toConsole", (req, res) => {
    //console.log(req.body.myMessage);
    res.send(req.body.myMessage);
});
app.post("/addElement", (req, res) => {
    let id1FromRequest = req.body.id;
    let newId = req.body.newId;
    let newTitle = req.body.newTitle;
    let newBody = req.body.newBody;
    if (id1FromRequest === "1" || id1FromRequest === "2") {
        //var1:
        // mockedData.lists[id1FromRequest-1].listElements.push({id: "4", title:"Do your homework",body:"Bla blaaaaaaaaaaaaaaaaaaaaaaa"});
        //var2: 
        mockedData.lists[id1FromRequest - 1].listElements.push({ id: newId, title: newTitle, body: newBody });
        res.send(mockedData.lists);
    }
    else {
        res.send("This id doesn't exist!");
    }
});
app.post("/deleteElement", (req, res) => {
    let id1FromRequest = req.body.id1;
    let id2FromRequest = req.body.id2;
    if (id1FromRequest === "1" || id2FromRequest === "2") {
        delete mockedData.lists[id1FromRequest - 1].listElements[id2FromRequest - 1];
        res.send(mockedData.lists);
    }
    else {
        res.send("This id doesn't exist!");
    }
});
app.get("/list/:id", (req, res) => {
    let idFromRequest = req.params.id;
    //let requestList = mockedData.lists.find(el => el.id === idFromRequest);
    let requestList = mockedData.lists[idFromRequest - 1];
    if (requestList) {
        res.send(requestList);
    }
    else {
        res.send("No list found.");
    }
});
app.get("/list/:id/:ElementId", (req, res) => {
    let id1FromRequest = req.params.id;
    let id2FromRequest = req.params.ElementId;
    let requestList = mockedData.lists[id1FromRequest - 1].listElements[id2FromRequest - 1];
    if (requestList) {
        res.send(requestList);
    }
    else {
        res.send("No list found.");
    }
});
app.listen(port, () => {
    console.log(`Server started at http://localhost:${port}!`);
});
//# sourceMappingURL=index.js.map