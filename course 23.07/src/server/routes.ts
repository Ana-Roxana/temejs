import express from "express";
import DriverManager from "../services/DriverManager";

export default (app:express.Application)=>{

app.get(
    "/",
    (request: express.Request, response: express.Response) => {
        response.send("Hello world");
    }
);

app.get(
    "/lists",
    async (req: express.Request, res: express.Response) => {
        try {
            let lists = await DriverManager.Instance.getAllToDoLists();
            if (lists.length > 0) {
                res.send(lists);
            } else {
                res.send("No list found!");
            }
        } catch {
            res.send("Error!");
        }
    }
);

app.get(
    "/list/:name",
    async (req: express.Request, res: express.Response) => {
        let nameFromRequest = req.params.name;

        try {
            let requestList = await DriverManager.Instance.getListByName(
                nameFromRequest	);
            res.send(requestList);
        } catch {
            res.send("Error!");
        }
    }
);

app.post(
    "/addList",
    async (req: express.Request, res: express.Response) => {
        try {
            await DriverManager.Instance.addNewList(req.body.name);
            res.send("Success!");
        } catch {
            res.send("Error!");
        }
    }
);

app.post(
    "/addListElement",
    async (req: express.Request, res: express.Response) => {
        try {
            await DriverManager.Instance.addNewElementForList(
                req.body.listName,
                req.body.taskName,
                req.body.description,
                
            );
            res.send("Success!");
        } catch {
            res.send("Error!");
        }
    }
);

app.delete("/:listName",async(req:express.Request,res:express.Response)=>{
    let nameFromRequest=req.params.listName;
    try{
      let deletedList=await DriverManager.Instance.deleteListByName(nameFromRequest);
      res.send(deletedList);
    }catch{
        res.send("Error!");
    }
});

app.delete("/:listName/:elementName",async(req:express.Request,res:express.Response)=>{
    let nameOfList=req.params.listName;
    let nameOfElement=req.params.elementName;
    try{
        let deletedElement=await DriverManager.Instance.deleteElementFromList(nameOfList,nameOfElement);
        res.send(deletedElement);
    }catch{
        res.send("Error!");
    }
});
}