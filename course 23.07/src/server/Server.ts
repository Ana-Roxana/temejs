import express from "express";
import bodyParser from "body-parser";
import DriverManager from "../services/DriverManager";
import cors = require("cors");

export default class Server {
	private port: number;
	private app: express.Application;

	public constructor(app: express.Application, port: number) {
		this.port = port;
		this.app = app;

		this.configApp();
		this.setRoutes();

		DriverManager.Instance.connect();

		this.startServer();
	}

	private startServer() {
		this.app.listen(this.port, () => {
			console.log(`Server started at http://localhost:${this.port}!`);
		});
	}

	private configApp() {
		// parse application/x-www-form-urlencoded from body
		this.app.use(bodyParser.json());

		//parse application.json from body
		this.app.use(bodyParser.urlencoded({ extended: false }));

		this.app.use(cors());
		
	}

	private setRoutes() {
		

	}
}
