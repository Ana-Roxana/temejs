import mongoose,{Schema,Document}from "mongoose";

interface IUser extends Document{
    username:string;
    password:string;
    email:string;
    validPasword(pasword:string):boolean;
}

const UserSchema=new Schema({
    username:{
        type:String,
        required:true,
        unique:true
    },

    password:{
        type:String,
        required:true

    },
    email:{type:String,required:true, unique:true}
},
{collection:"User"}
);

UserSchema.methods.validPasword=function(password:string){
    return password===this.password;
};

export default mongoose.model<IUser>("User",UserSchema);