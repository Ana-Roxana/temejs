import mongoose, { Schema, Document } from "mongoose";

export interface IListElement extends Document {
	title: string;
	body: string;
}

export const ListElementSchema = new Schema({
	title: {
		type: String,
		required: true
	},
	body: {
		type: String
	}
});

export default mongoose.model<IListElement>("ListElement", ListElementSchema);
